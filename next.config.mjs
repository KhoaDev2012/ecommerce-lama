/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        hostname: "img.buzzfeed.com",
      },
    ],
  },
};

export default nextConfig;
