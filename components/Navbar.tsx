import Link from "next/link";
import Image from "next/image";

import Menu from "@/components/Menu";
import NavIcons from "@/components/NavIcons";
import SearchBar from "@/components/SearchBar";

const Navbar = () => {
  return (
    <div className="relative h-20 px-4 md:px-8 lg:px-16 xl:px-32 2xl:px-64 bg-[#1db8da]">
      {/*   MOBILE SCREEN */}
      <div className="flex items-center justify-between h-full md:hidden">
        <Link href="/">
          <div className="text-2xl font-semibold tracking-wide text-white">
            STORE
          </div>
        </Link>
        <Menu />
      </div>

      <div className="hidden md:flex items-center justify-between gap-8 h-full">
        <div className="w-1/3 xl:w-1/2 flex items-center gap-12">
          <Link href="/" className="flex items-center gap-3">
            {/* <Image src="" alt="logo" width={24} height={24} /> */}
            <div className="text-2xl font-semibold tracking-wide text-white">
              STORE
            </div>
          </Link>
          <div className="hidden xl:flex gap-4">
            <Link href="/">Home</Link>
            <Link href="/">Shop</Link>
            <Link href="/">Deals</Link>
            <Link href="/">About</Link>
            <Link href="/">Contact</Link>
          </div>
        </div>
        <div className="w-2/3 xl:w-1/2 flex items-center justify-between gap-8">
          <SearchBar />
          <NavIcons />
        </div>
      </div>
    </div>
  );
};

export default Navbar;
