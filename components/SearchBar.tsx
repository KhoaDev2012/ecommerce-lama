"use client";

import { useRouter } from "next/navigation";
import { BsSearch } from "react-icons/bs";

const SearchBar = () => {
  const router = useRouter();
  const handleSearch = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const name = formData.get("name") as string;

    if (name) {
      router.push(`/list?name=${name}`);
    }
  };

  return (
    <form
      className="flex items-center justify-between gap-4 bg-gray-100 p-2 rounded-md flex-1 px-4"
      onSubmit={handleSearch}
    >
      <input
        className="flex-1 bg-transparent outline-none"
        type="text"
        placeholder="Search Product"
        name="name"
      />
      <button className="cursor-pointer">
        <BsSearch size={20} />
      </button>
    </form>
  );
};

export default SearchBar;
