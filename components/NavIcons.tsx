"use client";

import { BsBell } from "react-icons/bs";
import { FaRegCircleUser } from "react-icons/fa6";
import { BsCart3 } from "react-icons/bs";
import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import CartModal from "@/components/CartModal";

const NavIcons = () => {
  const [isProfileOpen, setIsProfileOpen] = useState(false);
  const [isCartOpen, setIsCartOpen] = useState(false);
  const router = useRouter();

  const isLoggedIn = false;

  const handleProfile = () => {
    if (!isLoggedIn) {
      router.push("/login");
    }
    setIsProfileOpen((prev) => !prev);
  };

  return (
    <div className="flex items-center gap-4 text-white xl:gap-6 cursor-pointer relative">
      <FaRegCircleUser size={20} onClick={handleProfile} />
      {isProfileOpen && (
        <div className="absolute p-4 rounded-md top-8 left-0 text-sm drop-shadow-sm  bg-slate-500 z-20">
          <Link href="/">Profile</Link>
          <div className="mt-2 cursor-pointer">Logout</div>
        </div>
      )}
      <BsBell size={20} />
      <div className="relative cursor-pointer">
        <BsCart3 size={20} onClick={() => setIsCartOpen((prev) => !prev)} />
        <div className="absolute w-6 h-6 -top-4 -right-4 bg-[#fb7529] rounded-lg text-sm flex items-center justify-center">
          2
        </div>
      </div>
      {isCartOpen && <CartModal />}
    </div>
  );
};

export default NavIcons;
