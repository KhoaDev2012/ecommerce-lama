"use client";

import Image from "next/image";

const CartModal = () => {
  const cartItems = true;

  return (
    <div className="w-max absolute p-4 rounded-md bg-white text-black top-10 right-0 flex flex-col gap-6 z-20 shadow-[0_3px_10px_rgba(0,0,0,0.2)] transition">
      {!cartItems ? (
        <div>No have any item</div>
      ) : (
        <>
          <div className="flex flex-col gap-2">
            <h1 className="font-semibold">Shopping Cart</h1>

            <div className="flex items-center justify-between gap-4">
              <Image
                src="https://img.buzzfeed.com/buzzfeed-static/static/2023-07/26/23/asset/91b06d594cf5/sub-buzz-1205-1690414369-2.jpg?crop=1810:1140;100,570&resize=625:*&output-format=jpg&output-quality=auto"
                width={100}
                height={100}
                className="object-cover rounded-md border border-slate-700"
                alt=""
              />

              <div className="flex flex-col justify-between w-full gap-6">
                {/* TOP */}
                <div>
                  {/* TITLE */}
                  <div className="flex gap-8">
                    <h3 className="font-semibold">Product Name</h3>
                    <div className="bg-sky-100 p-1 rounded-md">$49</div>
                  </div>

                  <div className="text-sm text-neutral-500">available</div>
                </div>

                {/* BOTTOM */}
                <div className="flex items-center justify-between text-sm ">
                  <span>Quantity 2</span>
                  <span className="text-sky-600">Remove</span>
                </div>
              </div>
            </div>

            <div className="flex items-center justify-between gap-4">
              <Image
                src="https://img.buzzfeed.com/buzzfeed-static/static/2023-07/26/23/asset/91b06d594cf5/sub-buzz-1205-1690414369-2.jpg?crop=1810:1140;100,570&resize=625:*&output-format=jpg&output-quality=auto"
                width={100}
                height={100}
                className="object-cover rounded-md border border-slate-700"
                alt=""
              />

              <div className="flex flex-col justify-between w-full gap-6">
                {/* TOP */}
                <div>
                  {/* TITLE */}
                  <div className="flex gap-8">
                    <h3 className="font-semibold">Product Name</h3>
                    <div className="bg-sky-100 p-1 rounded-md">$49</div>
                  </div>

                  <div className="text-sm text-neutral-500">available</div>
                </div>

                {/* BOTTOM */}
                <div className="flex items-center justify-between text-sm ">
                  <span>Quantity 2</span>
                  <span className="text-sky-600">Remove</span>
                </div>
              </div>
            </div>
          </div>

          <div>
            <div className="flex items-center justify-between font-semibold">
              <span>Subtotal</span>
              <span>$49</span>
            </div>
            <p className="text-neutral-500 mt-2 mb-4">
              Shipping and texes calculated at checkbox
            </p>
            <div className="flex justify-between items-center text-sm">
              <button className="py-2 px-4 border-[2px] border-sky-900 text-sky-900 rounded-md hover:opacity-80 text-md font-semibold transition">
                View Cart
              </button>
              <button className="py-2 px-4 bg-sky-800 text-white rounded-md hover:bg-opacity-80 text-md font-medium transition">
                Checkout
              </button>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default CartModal;
